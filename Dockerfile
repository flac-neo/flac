FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > flac.log'

COPY flac .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' flac
RUN bash ./docker.sh

RUN rm --force --recursive flac
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD flac
